//
//  MemorizeApp.swift
//  Memorize
//
//  Created by Sergio Andres Rodriguez Castillo on 30/01/24.
//

import SwiftUI

@main
struct MemorizeApp: App {
    @StateObject var game = EmojiMemoryGame()
    
    var body: some Scene {
        WindowGroup {
            EmojiMemoryGameView(viewModel: game)
        }
    }
}
